﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application.Dtos;
using Application.interfaces;
using Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MovieController : ControllerBase
    {
        private readonly IMovieService _movieService;

        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var movies = _movieService.GetAllMovies();
                if (movies == null) return NotFound("Not found");

                return Ok(movies);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            try
            {
                var movie = _movieService.GetMovieById(id);
                if (movie == null) return NotFound("Not found");

                return Ok(movie);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }

        [HttpPost]
        public IActionResult Post(MovieDto movie)
        {
            try
            {
                var newMovie = _movieService.AddMovie(movie);
                if (newMovie == null) return BadRequest("Error");

                return Ok(newMovie);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, MovieDto movie)
        {
            try
            {
                var newMovie = _movieService.UpdateMovie(id, movie);
                if (newMovie == null) return BadRequest("Error");

                return Ok(newMovie);
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                return _movieService.DeleteMovie(id) ?
                        Ok("Deleted") :
                        BadRequest("Error");
            }
            catch (Exception e)
            {
                return this.StatusCode(StatusCodes.Status500InternalServerError, $"Error: {e.Message}");
            }
        }
    }
}
