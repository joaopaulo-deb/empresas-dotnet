using AutoMapper;
using Application.Dtos;
using Entities;
using Entities.Identity;

namespace API.Helpers
{
    public class DataProfile : Profile
    {
        public DataProfile()
        {
            CreateMap<Movie, MovieDto>().ReverseMap();
            
            CreateMap<User, UserDto>().ReverseMap();
            CreateMap<User, UserLoginDto>().ReverseMap();
            CreateMap<User, UserUpdateDto>().ReverseMap();
        }
    }
}