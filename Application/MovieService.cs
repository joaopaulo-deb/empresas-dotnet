using System;
using Application.Dtos;
using Application.interfaces;
using AutoMapper;
using Entities;
using Infrastructure.interfaces;

namespace Application
{
    public class MovieService : IMovieService
    {
        private readonly IDefaultPersistence _defaultPersistence;
        private readonly IMoviePersistence _moviePersistence;
        private readonly IMapper _mapper;

        public MovieService(IDefaultPersistence DefaultPersistence,
                            IMoviePersistence MoviePersistence,
                            IMapper Mapper)
        {
            _defaultPersistence = DefaultPersistence;
            _moviePersistence = MoviePersistence;
            _mapper = Mapper;
        }

        public MovieDto AddMovie(MovieDto model)
        {
            try
            {
                var movie = _mapper.Map<Movie>(model);
                
                _defaultPersistence.Add<Movie>(movie);

                if (_defaultPersistence.SaveChanges())
                {
                    var result = _moviePersistence.GetMovieById(movie.Id);

                    return _mapper.Map<MovieDto>(result);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public MovieDto UpdateMovie(int MovieId, MovieDto model)
        {
            try
            {
                var movie = _moviePersistence.GetMovieById(MovieId);
                if (movie == null) return null;

                model.Id = movie.Id;

                _mapper.Map(model, movie);

                _defaultPersistence.Update<Movie>(movie);

                if (_defaultPersistence.SaveChanges())
                {
                    var result = _moviePersistence.GetMovieById(movie.Id);

                    return _mapper.Map<MovieDto>(result);
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool DeleteMovie(int MovieId)
        {
            /*try
            {
                var movie = _moviePersistence.GetMovieById(movieId);
                if (movie == null) throw new Exception("Movie para delete não encontrado.");

                _defaultPersistence.Delete<Movie>(movie);
                return _geralPersistenceSaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }*/
            return true;
        }

        public MovieDto[] GetAllMovies()
        {
            try
            {
                var movies = _moviePersistence.GetAllMovies();
                if (movies == null) return null;

                 var result = _mapper.Map<MovieDto[]>(movies);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public MovieDto GetMovieById(int MovieId)
        {
            try
            {
                var movie = _moviePersistence.GetMovieById(MovieId);
                if (movie == null) return null;

                var result = _mapper.Map<MovieDto>(movie);

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}