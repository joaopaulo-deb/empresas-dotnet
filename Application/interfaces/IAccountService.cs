using System.Threading.Tasks;
using Application.Dtos;
using Entities;
using Microsoft.AspNetCore.Identity;

namespace Application.interfaces
{
    public interface IAccountService
    {
        Task<bool> UserExists(string username);
        UserUpdateDto GetUserByUserNameAsync(string username);
        Task<SignInResult> CheckUserPasswordAsync(UserUpdateDto userUpdateDto, string password);
        Task<UserUpdateDto> CreateAccountAsync(UserDto userDto);
    }
}