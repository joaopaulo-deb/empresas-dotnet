using Application.Dtos;

namespace Application.interfaces
{
    public interface IMovieService
    {
         MovieDto AddMovie(MovieDto movie);
         MovieDto UpdateMovie(int movieId, MovieDto newMovie);
         bool DeleteMovie(int movieId);
         MovieDto[] GetAllMovies();
         MovieDto GetMovieById(int movieId);
    }
}