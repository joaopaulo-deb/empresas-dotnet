using System.Threading.Tasks;
using Application.Dtos;

namespace Application.interfaces
{
    public interface ITokenService
    {
        Task<string> CreateToken(UserUpdateDto userUpdateDto);
    }
}