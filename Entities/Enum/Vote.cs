namespace Entities.Enum
{
    public enum Vote
    {
        Zero,
        One,
        Two,
        Three,
        Four
    }
}