using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace Entities.Identity
{
    public class User : IdentityUser<int>
    {
        public IEnumerable<UserRole> UserRoles { get; set; }
    }
}