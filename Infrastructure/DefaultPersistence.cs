using Infrastructure.Context;
using Infrastructure.interfaces;

namespace Infrastructure
{
    public class DefaultPersistence : IDefaultPersistence
    {
        private readonly DataContext _context;

        public DefaultPersistence(DataContext context)
        {
            _context = context;            
        }

        public void Add<T>(T entity) where T : class
        {
             _context.Add(entity);
        }

        public void Update<T>(T entity) where T : class
        {
             _context.Update(entity);
        }

        public bool SaveChanges()
        {
            return _context.SaveChanges() > 0;
        }
    }
}