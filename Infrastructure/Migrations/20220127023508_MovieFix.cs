﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infrastructure.Migrations
{
    public partial class MovieFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataEvento",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Movies");

            migrationBuilder.DropColumn(
                name: "QtdPessoas",
                table: "Movies");

            migrationBuilder.RenameColumn(
                name: "Tema",
                table: "Movies",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "Telefone",
                table: "Movies",
                newName: "Genre");

            migrationBuilder.RenameColumn(
                name: "Local",
                table: "Movies",
                newName: "Director");

            migrationBuilder.RenameColumn(
                name: "ImagemURL",
                table: "Movies",
                newName: "Actors");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Movies",
                newName: "Tema");

            migrationBuilder.RenameColumn(
                name: "Genre",
                table: "Movies",
                newName: "Telefone");

            migrationBuilder.RenameColumn(
                name: "Director",
                table: "Movies",
                newName: "Local");

            migrationBuilder.RenameColumn(
                name: "Actors",
                table: "Movies",
                newName: "ImagemURL");

            migrationBuilder.AddColumn<DateTime>(
                name: "DataEvento",
                table: "Movies",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Movies",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "QtdPessoas",
                table: "Movies",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
