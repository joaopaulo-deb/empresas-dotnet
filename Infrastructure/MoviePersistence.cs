using System.Linq;
using Entities;
using Infrastructure.Context;
using Infrastructure.interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    public class MoviePersistence : DefaultPersistence, IMoviePersistence
    {
        private readonly DataContext _context;

        public MoviePersistence(DataContext context) : base(context)
        {
            _context = context;
        }

        public Movie[] GetAllMovies()
        {
            IQueryable<Movie> query = _context.Movies;
            query = query.AsNoTracking();

            return query.ToArray();
        }

        public Movie GetMovieById(int userId)
        {
            IQueryable<Movie> query = _context.Movies;
            query = query.AsNoTracking();

            return query.FirstOrDefault();
        }
    }
}