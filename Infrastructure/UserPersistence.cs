using System.Collections.Generic;
using System.Linq;
using Entities.Identity;
using Infrastructure.Context;
using Infrastructure.interfaces;

namespace Infrastructure
{
    public class UserPersistence : DefaultPersistence, IUserPersistence
    {
        private readonly DataContext _context;

        public UserPersistence(DataContext context) : base(context)
        {
            _context = context;
        }

        public User GetUserById(int Id)
        {
            return _context.Users.Find(Id);
        }

        public IEnumerable<User> GetUsers()
        {
            return _context.Users.ToList();
        }

        public User GetUserByUserName(string userName)
        {
            return _context.Users.SingleOrDefault(user => user.UserName == userName.ToLower());
        }
    }
}