using Entities;

namespace Infrastructure.interfaces
{
    public interface IMoviePersistence : IDefaultPersistence
    {
        Movie[] GetAllMovies(); 
        Movie GetMovieById(int movieId);
    }
}