using System.Collections.Generic;
using Entities.Identity;

namespace Infrastructure.interfaces
{
    public interface IUserPersistence : IDefaultPersistence
    {
         IEnumerable<User> GetUsers();
         User GetUserById(int Id);
         public User GetUserByUserName(string userName);
    }
}